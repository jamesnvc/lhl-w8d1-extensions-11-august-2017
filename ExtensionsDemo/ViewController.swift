//
//  ViewController.swift
//  ExtensionsDemo
//
//  Created by James Cash on 11-08-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import SharedStuff

class ViewController: UIViewController {

    @IBOutlet var savedValueSlider: UISlider!
    @IBOutlet var savedValueLabel: UILabel!

    var savedValue: Float = 2.5

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func valueChanged(_ sender: UISlider) {
        savedValue = sender.value
        savedValueLabel.text = "\(savedValue)"
        saveValue(savedValue)
    }

    func restoreValue() {
        savedValue = getValue()
        savedValueLabel.text = "\(savedValue)"
        savedValueSlider.value = savedValue
    }

}

