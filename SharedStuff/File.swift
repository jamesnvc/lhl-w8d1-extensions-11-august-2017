//
//  File.swift
//  ExtensionsDemo
//
//  Created by James Cash on 11-08-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import Foundation

let savedValueKey = "ourappsharedkey"
let defaultsSuiteName = "group.occasionallycogent.testing"

public func saveValue(_ savedValue: Float) {
    UserDefaults(suiteName: defaultsSuiteName)!.set(savedValue, forKey: savedValueKey)
}

public func getValue() -> Float {
    return UserDefaults(suiteName: defaultsSuiteName)!.float(forKey: savedValueKey)
}
