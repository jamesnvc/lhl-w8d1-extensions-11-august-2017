//
//  SharedStuff.h
//  SharedStuff
//
//  Created by James Cash on 11-08-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SharedStuff.
FOUNDATION_EXPORT double SharedStuffVersionNumber;

//! Project version string for SharedStuff.
FOUNDATION_EXPORT const unsigned char SharedStuffVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SharedStuff/PublicHeader.h>


