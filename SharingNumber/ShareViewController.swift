//
//  ShareViewController.swift
//  SharingNumber
//
//  Created by James Cash on 11-08-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import SharedStuff

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        guard let newValue = NumberFormatter().number(from: contentText) else {
            return false
        }
        let val = newValue.floatValue
        return 0 <= val && val < 5
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.

        let val = NumberFormatter().number(from: contentText)!.floatValue
        saveValue(val)
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
