//
//  TodayViewController.swift
//  ShowingNumberToday
//
//  Created by James Cash on 11-08-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import SharedStuff

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet var numberStepper: UIStepper!
    @IBOutlet var numberLabel: UILabel!
    var currentValue: Float = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        currentValue = getValue()
        numberLabel.text = "\(currentValue)"
        numberStepper.value = Double(currentValue)
    }
    
    @IBAction func changeValue(_ sender: UIStepper) {
        currentValue = Float(sender.value)
        numberLabel.text = "\(currentValue)"
        saveValue(currentValue)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
